import tensorflow as tf 
import cma
from q_network import DQN, modified_sigmoid, fourier_basis
import gym
import numpy as np
import pickle

cartpole_setup = [{"force" : 10.0, "pole_length" : 0.5, "masscart" : 1.0, "masspole" : 0.1},
            {"force" : 5.0, "pole_length" : 0.25, "masscart" : 2.0, "masspole" : 0.2},
            {"force" : 20.0, "pole_length" : 1.2, "masscart" : 5.0, "masspole" : 0.1}#,
            # {"force" : 5.0, "pole_length" : 1.0, "masscart" : 2.0, "masspole" : 0.5},
            # {"force" : 10.0, "pole_length" : 0.5, "masscart" : 5.0, "masspole" : 1.0}
            ]


class MetaNet:
   REPLAY_MEMORY_SIZE = 10000
   RANDOM_ACTION_DECAY = 0.99
   MIN_RANDOM_ACTION_PROB = 0.1
   HIDDEN1_SIZE = 20
   HIDDEN2_SIZE = 20
   LEARNING_RATE = 0.01
   MINIBATCH_SIZE = 30
   DISCOUNT_FACTOR = 0.9
   TARGET_UPDATE_FREQ = 300
   REG_FACTOR = 0.001
   LOG_DIR = './tmp/'


   def __init__(self, input_size, output_size):

      self.input_size = input_size #self.env.observation_space.shape[0]
      self.output_size = output_size
      self.session = tf.Session()
      self.DQN = None
      self.best_score = float("-inf")
      self.best_params = None
      self.best_history = None
      self.init_network()
      self.iter = 0
      self.progress_hist = []

      
   def init_network(self):
      # Inference
      self.x = tf.placeholder(tf.float32, [None, self.input_size])
      with tf.name_scope('hidden1_meta'):
         self.W1 = tf.Variable(
                         tf.truncated_normal([self.input_size, self.HIDDEN1_SIZE], 
                         stddev=0.01), name='W1_meta')
         self.b1 = tf.Variable(tf.zeros(self.HIDDEN1_SIZE), name='b1_meta')
         h1 = tf.nn.tanh(tf.matmul(self.x, self.W1) + self.b1)
      with tf.name_scope('hidden2_meta'):
         self.W2 = tf.Variable(
                         tf.truncated_normal([self.HIDDEN1_SIZE, self.HIDDEN2_SIZE], 
                         stddev=0.01), name='W2_meta')
         self.b2 = tf.Variable(tf.zeros(self.HIDDEN2_SIZE), name='b2_meta')
         h2 = tf.nn.tanh(tf.matmul(h1, self.W2) + self.b2)
      with tf.name_scope('output_meta'):
         self.W3 = tf.Variable(
                         tf.truncated_normal([self.HIDDEN2_SIZE, self.output_size], 
                         stddev=0.01), name='W3_meta')
         self.b3 = tf.Variable(tf.zeros(self.output_size), name='b3_meta')
         self.Pa = tf.nn.softmax(tf.matmul(h2, self.W3) + self.b3)
      self.weights = [self.W1, self.b1, self.W2, self.b2, self.W3, self.b3]

   def predict(self, features):
      pred = self.session.run(self.Pa, feed_dict={self.x: [features]})
      return pred[0]


   def set_parameters(self, x):
      w1idx = self.input_size*self.HIDDEN1_SIZE
      self.session.run(tf.assign(self.W1, np.asarray(x[:w1idx]).reshape((self.input_size, self.HIDDEN1_SIZE))))
      b1idx = w1idx+self.HIDDEN1_SIZE
      self.session.run(tf.assign(self.b1, np.asarray(x[w1idx:b1idx]).reshape((self.HIDDEN1_SIZE,))))

      w2idx = b1idx+self.HIDDEN1_SIZE*self.HIDDEN2_SIZE
      self.session.run(tf.assign(self.W2, np.asarray(x[b1idx:w2idx]).reshape((self.HIDDEN1_SIZE, self.HIDDEN2_SIZE))))
      b2idx = w2idx+self.HIDDEN2_SIZE
      self.session.run(tf.assign(self.b2, np.asarray(x[w2idx:b2idx]).reshape((self.HIDDEN2_SIZE,))))
      
      w3idx = b2idx+self.HIDDEN2_SIZE*self.output_size
      self.session.run(tf.assign(self.W3, np.asarray(x[b2idx:w3idx]).reshape((self.HIDDEN2_SIZE, self.output_size))))
      b3idx = w3idx+self.output_size
      self.session.run(tf.assign(self.b3, np.asarray(x[w3idx:b3idx]).reshape((self.output_size,))))

   def loss(self, x):
      self.set_parameters(x)

      returns = []
      env_trial_returns = []
      for i, setup in enumerate(cartpole_setup):
         env = gym.make('CartPole-v0')
         env.env.force_mag = setup["force"]
         env.env.length = setup["pole_length"]
         env.env.masscart = setup["masscart"]
         env.env.masspole = setup["masspole"]

         num_trials = 2
         avg_return = 0.0
         trial_returns = []
         for trial in range(num_trials):
            print("Running setup " + str(i) + " - trial " + str(trial))
            if self.DQN is None:
               self.DQN = DQN(env, session=self.session, meta_network=self)
            else:
               self.DQN.reset_variables(env)

            # dqn.env.monitor.start('/tmp/cartpole', force=True)
            disc_returns = self.DQN.train(log_dir=self.LOG_DIR+"/setup_"+str(i)+"_trial_"+str(trial), verbose=True)
            trial_returns.append( disc_returns )
            avg_return += sum(disc_returns)

         env_trial_returns.append( trial_returns )
         avg_return /= num_trials
         returns.append(avg_return)

      score = np.mean(returns)
      print( score )
      if score > self.best_score:
         self.best_score = score
         self.best_history = list(env_trial_returns)
         self.best_params = x

      if self.iter % 2 == 0:
         self.progress_hist.append( self.best_history )

      self.iter += 1
      return -score


   def train_cma(self):
      num_params = (self.input_size*self.HIDDEN1_SIZE)+self.HIDDEN1_SIZE + \
                     (self.HIDDEN1_SIZE*self.HIDDEN2_SIZE)+self.HIDDEN2_SIZE + \
                     (self.HIDDEN2_SIZE*self.output_size)+self.output_size

      params = np.random.random( (num_params, ))
      maxiter = 2
      result = cma.fmin(objective_function=self.loss, x0=params, sigma0=2.0, options={'maxiter': maxiter, 'popsize': 2})

      self.set_parameters(self.best_params)

      pickle.dump((self.progress_hist, self.best_params), open("progress_history.pkl", "wb"))



if __name__ == '__main__':
   env = gym.make('CartPole-v0')
   env.reset()
   (obs, reward, done, info) = env.step(env.action_space.sample())
   obs = modified_sigmoid(obs)
   phi = fourier_basis(obs, order=1)

   input_size = phi.shape[0] #self.env.observation_space.shape[0]
   output_size = env.action_space.n

   meta_net = MetaNet(input_size, output_size)
   meta_net.train_cma()

   

   


