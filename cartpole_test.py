import gym
from q_network import DQN
from linear_agent import LinearAgent
import cma

import matplotlib.pyplot as plt
import numpy as np
import os
import math
import itertools
import matplotlib.pyplot as plt
import pickle

from threading import Thread
from LinearMeta import MetaPolicy

best_value = 0.0
best_params = None
best_rewards = None


class EnvWrapper:

    def __init__(self, env, basis_order=2):
        self.env = env
        self.env.reset()
        self.order = basis_order
        (obs, reward, done, info) = self.env.step(self.env.action_space.sample())
        obs = self.modified_sigmoid(obs)
        phi = fourier_basis(obs, order=self.order)
        self.num_features = phi.shape[0]
        self.num_actions = self.env.action_space.n

        
    def modified_sigmoid(self, x):
        #return x normalized 0-1
        #Find values to normalize with sigmoid in range
        xout = x.copy()
        xout[0] = 1 / (1 + np.exp(-(0.5*x[0])))
        xout[1] = 1 / (1 + np.exp(-(0.002*x[1])))
        xout[2] = 1 / (1 + np.exp(-(5.0*x[2])))
        xout[3] = 1 / (1 + np.exp(-(0.002*x[3])))

        return xout

    def reset(self):
        obs = self.env.reset()
        obs = self.modified_sigmoid(obs)
        obs = fourier_basis(obs, order=self.order)
        return obs

    def render(self):
        self.env.render()

    def random_action(self):
        return self.env.action_space.sample()

    def step(self, action=None):
        if action is None:
            obs, reward, done, _ = self.env.step( self.env.action_space.sample() )
        else:
            obs, reward, done, _ = self.env.step(action)
        
        obs = self.modified_sigmoid(obs)
        obs = fourier_basis(obs, order=self.order)
        return obs, reward, done

        
def thread_train(meta, alpha, idx, sample_rewards, setup, order):
    print("Starting thread " + str(idx))
    gym_env = gym.make('CartPole-v0')
    gym_env.env.force_mag = setup["force"]
    gym_env.env.length = setup["pole_length"]
    gym_env.env.masscart = setup["masscart"]
    gym_env.env.masspole = setup["masspole"]

    env = EnvWrapper(gym_env, basis_order=order)
    agent = LinearAgent(env, meta_policy=meta)
    agent.reset_variables(env, alpha=alpha, meta_policy=meta)
    rewards = agent.train(verbose=False)
    sample_rewards[idx] = rewards
    print("Thread " + str(idx) + " finished.") 



def sanity_check(cartpole_setup):
    agent, meta = None, None
    # alphas = [0.01, 0.005, 0.001, 0.0005, 0.0001, 5e-5, 1e-5]
    # basis_order = range(1, 5)

    alphas = [5e-5]
    basis_order = [3]
    for k in range(10, 20):
        all_samples = []
        meta_desc = "no_meta_" if k == 0 else "meta_"+str(k)+"_"
        base_dir = os.path.dirname(os.path.realpath(__file__))
        domain_dir = base_dir+"/linear_figures/"+meta_desc+"/"
        if os.path.isdir(domain_dir) == False:
            os.mkdir(domain_dir)

        print("Testing domain: " + domain_dir)
        for i, setup in enumerate(cartpole_setup):
            gym_env = gym.make('CartPole-v0')
            gym_env.env.force_mag = setup["force"]
            gym_env.env.length = setup["pole_length"]
            gym_env.env.masscart = setup["masscart"]
            gym_env.env.masspole = setup["masspole"]

            for alpha in alphas:
                for order in basis_order:
        
                    env = EnvWrapper(gym_env, basis_order=order)
                    if i == 0:
                        theta = (np.random.random( (env.num_actions, env.num_features) ) * 40.0) - 20.0
                        meta = MetaPolicy(num_features=env.num_features, num_actions=env.num_actions, init_theta=theta)

                    if k == 0:
                        meta = None

                    num_samples = 5
                    sample_rewards = [ None ] * num_samples

                    #Environments being created per thread. Delete previous environment creation.
                    threads = [ Thread(target=thread_train, args=(meta, alpha, idx, sample_rewards, setup, order)) for idx in range(num_samples)]

                    for t in threads:
                        t.start()

                    for t in threads:
                        t.join()
                   
                    ma_rewards = []
                    for r in sample_rewards:
                        ma = [ np.mean(r[j*10: (j+1)*10]) for j in range(len(r) // 10) ]
                        ma_rewards.append( ma )

                    sample_rewards = np.asarray(ma_rewards)

                    mean_rewards = np.mean(sample_rewards, axis=0)
                    fig = plt.figure()
                    ax = fig.add_subplot(111)
                    ax.set_ylim([0, 100])
                    ax.plot(range(len(mean_rewards)), mean_rewards, color="blue")
                    ax.fill_between(range(len(mean_rewards)), mean_rewards+np.std(sample_rewards, axis=0),
                                        mean_rewards-np.std(sample_rewards, axis=0), facecolor="blue", edgecolor="blue", alpha=0.4, interpolate=True)

                    all_samples.append(sample_rewards.copy()) 
                    fig.savefig(domain_dir+"domain_"+str(i)+"_alpha_"+str(alpha)+"_order_"+str(env.order)+".png")
                    plt.close('all')
            
        print("Savings sample rewards in " + domain_dir)
        pickle.dump(all_samples, open(domain_dir+"rewards.pkl", "wb"))



def _error_func(x, cartpole_setup, baseline_rewards):
    
    alpha = 5e-5
    basis_order = 3
    domain_rewards = []
    for i, setup in enumerate(cartpole_setup):
        gym_env = gym.make('CartPole-v0')
        gym_env.env.force_mag = setup["force"]
        gym_env.env.length = setup["pole_length"]
        gym_env.env.masscart = setup["masscart"]
        gym_env.env.masspole = setup["masspole"]

    
        env = EnvWrapper(gym_env, basis_order=basis_order)
        theta = np.asarray(x).reshape( (env.num_actions, env.num_features) )
        theta = (theta * 40.0) - 20.0
        
        meta = MetaPolicy(num_features=env.num_features, num_actions=env.num_actions, init_theta=theta)


        num_samples = 5
        sample_rewards = [ None ] * num_samples

        #Environments being created per thread. Delete previous environment creation.
        threads = [ Thread(target=thread_train, args=(meta, alpha, idx, sample_rewards, setup, basis_order)) for idx in range(num_samples)]

        for t in threads:
            t.start()

        for t in threads:
            t.join()
        
        ma_rewards = []
        for r in sample_rewards:
            ma = [ np.mean(r[j*10: (j+1)*10]) for j in range(len(r) // 10) ]
            ma_rewards.append( ma )

        sample_rewards = np.asarray(ma_rewards)
        domain_rewards.append( sample_rewards.copy() )

    objective = 0.0
    for i in range(len(domain_rewards)):
        mean_rewards = np.mean(rewards[i], axis=0)
        ratios = mean_rewards / baseline_rewards[i]
        objective += np.sum(ratios)

    global best_value
    global best_params
    global best_rewards
    if objective > best_value:
        print("New best value: " + str(best_value))
        best_value = objective
        best_params = theta.copy()
        best_rewards = list(domain_rewards)

    return -objective



def optimize(cartpole_setup, baseline_rewards):
    agent, meta = None, None
    # alphas = [0.01, 0.005, 0.001, 0.0005, 0.0001, 5e-5, 1e-5]
    # basis_order = range(1, 5)

    base_dir = os.path.dirname(os.path.realpath(__file__))
    domain_dir = base_dir+"/linear_figures/optimize/"
    if os.path.isdir(domain_dir) == False:
        os.mkdir(domain_dir)

    gym_env = gym.make('CartPole-v0')
    env = EnvWrapper(gym_env, basis_order=3)

    params = np.random.random( (env.num_actions*env.num_features, )).tolist()
    sigma = 0.2

    result = cma.fmin(objective_function=_error_func, x0=params, sigma0=sigma, args=(cartpole_setup, baseline_rewards), options={'maxiter': 10, 'popsize': 10}, bipop=True, restarts=1)

    global best_params
    global best_rewards
    global best_value

    
    print("Savings reward plots in " + domain_dir)
    for k, mean_rewards in enumerate(best_rewards):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_ylim([0, 100])
        ax.plot(range(len(mean_rewards)), mean_rewards, color="blue")
        ax.fill_between(range(len(mean_rewards)), mean_rewards+np.std(sample_rewards, axis=0),
                            mean_rewards-np.std(sample_rewards, axis=0), facecolor="blue", edgecolor="blue", alpha=0.4, interpolate=True)

        all_samples.append(sample_rewards.copy()) 
        fig.savefig(domain_dir+"domain_"+str(k)+".png")
        plt.close('all')
        
    
    print("Best value found: " + str(best_value))
    pickle.dump(best_rewards, open(domain_dir+"rewards.pkl", "wb"))




if __name__ == '__main__':
    cartpole_setup = [{"force" : 10.0, "pole_length" : 0.5, "masscart" : 1.0, "masspole" : 0.1},
                {"force" : 5.0, "pole_length" : 0.25, "masscart" : 2.0, "masspole" : 0.2},
                {"force" : 20.0, "pole_length" : 1.2, "masscart" : 5.0, "masspole" : 0.1},
                {"force" : 5.0, "pole_length" : 1.0, "masscart" : 2.0, "masspole" : 0.5},
                {"force" : 10.0, "pole_length" : 0.5, "masscart" : 5.0, "masspole" : 1.0}
                ]

    ###############################
    # sanity_check(cartpole_setup)


    ################################
    baseline_rewards = []
    ideal_value = 0.0
    no_meta_rewards = pickle.load(open("./linear_figures/no_meta_/rewards.pkl", "rb"))

    for i in range(len(no_meta_rewards)):
        mean_rewards = np.mean(no_meta_rewards[i], axis=0)
        baseline_rewards.append( np.max(mean_rewards) )

    optimize(cartpole_setup, baseline_rewards)
