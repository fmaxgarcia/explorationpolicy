

from LinearMeta import MetaPolicy
from Features import fourier_basis
from Environments import EnvWrapper
from CustomEnvironments.AnimatEnv import AnimatEnv 
from linear_agent import LinearAgent
import time
import matplotlib.pyplot as plt
import numpy as np

basis_order = 3
alpha = 0.0001

env = AnimatEnv("./CustomEnvironments/maze1.txt")
env.reset()

(obs, reward, done, info) = env.step(env.action_space.sample())
obs = EnvWrapper.normalize_range(obs, env.env_range)
phi = fourier_basis(obs, order=basis_order)

num_features = phi.shape[0]# + len( cartpole_setup[0].keys() )
num_actions = env.action_space.n


meta = MetaPolicy(num_features=num_features, num_actions=num_actions)

gym_env = AnimatEnv("./CustomEnvironments/maze1.txt")

env = EnvWrapper(gym_env, basis_order=basis_order, normalization=1)
agent = LinearAgent(env, meta_policy=meta, alpha=alpha)


rewards = agent.train(num_episodes=1000, max_steps=1000, verbose=True, update_meta=False, render=False)

rewards = [ np.mean(rewards[i*10:(i+1)*10]) for i in range(len(rewards)/10) ]
plt.plot(range(len(rewards)), rewards)
plt.show(block=True)
# for _ in range(10000):
#     reward, done, update_info = agent.perform_step(update_meta=False)
#     env.render()
#     time.sleep(1.0)
