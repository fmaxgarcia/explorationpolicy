
# from ExperimentsCartpole import *
# from ExperimentsLander import *
from ExperimentsAnimat import *



if __name__ == '__main__':
    
    ##### CartPole #######################
    
    # experiment_train_meta(save_directory="./MetaTraining/", eligibility_traces=True)
    # experiment_train_meta(save_directory="./MetaTrainingParallel/", eligibility_traces=False)

    # meta = pickle.load(open(sys.argv[1], "rb"))['meta']
    # experiment_evaluate_fixed_meta(save_directory="./TestingMeta/", meta=meta)

    # experiment_evaluate_exploit_learning(save_directory="./TestingExploitationEvaluation/")
    # experiment_evaluate_exploration_learning(save_directory="./TestingExplorationEvaluation/")

    #######################################

    ##### Lander #######################
    
    # experiment_train_meta(save_directory="./MetaTrainingLander", eligibility_traces=True)
    # experiment_train_meta(save_directory="./MetaTrainingParallelLander", eligibility_traces=False)

    # meta = pickle.load(open(sys.argv[1], "rb"))['meta']
    # experiment_evaluate_fixed_meta(save_directory="./TestingMetaLander/", meta=meta)

    # experiment_evaluate_exploit_learning(save_directory="./TestingExploitationLanderEvaluation/")
    # experiment_evaluate_exploration_learning(save_directory="./TestingExplorationLanderEvaluation/")

    #######################################
            
    ##### Animat #######################
    
    experiment_train_meta(save_directory="./MetaTrainingAnimat", eligibility_traces=True)
    experiment_train_meta(save_directory="./MetaTrainingParallelAnimat", eligibility_traces=False)

    # meta = pickle.load(open(sys.argv[1], "rb"))['meta']
    # experiment_evaluate_fixed_meta(save_directory="./TestingMetaLander/", meta=meta)

    # experiment_evaluate_exploit_learning(save_directory="./TestingExploitationLanderEvaluation/")
    # experiment_evaluate_exploration_learning(save_directory="./TestingExplorationLanderEvaluation/")

    #######################################
    