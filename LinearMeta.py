import numpy as np
from LearningAlgorithms.Sarsa import Sarsa

class MetaPolicy:

    def __init__(self, num_features, num_actions, init_theta=None):
        self.learning_algorithm = Sarsa(num_actions, num_features, init_theta, gamma=1.0, lambda_=0.9, alpha=1e-5)


    def predict(self, state, domain_state=None):
        return self.learning_algorithm.select_action(state, stochastic=True)

    def sarsa_update_single(self, state, action, reward, state_next, action_next, domain_state=None):
        self.learning_algorithm.update(state, action, reward, state_next, action_next, etraces=True)

    def sarsa_update(self, sample_updates):
        self.learning_algorithm.update_batch(sample_updates)
            
