import numpy as np 
import random
import math 
from sys import stdout

from LearningAlgorithms.Sarsa import Sarsa
from LearningAlgorithms.QLearning import QLearning
from LearningAlgorithms.REINFORCE import REINFORCE

class LinearAgent:
    RANDOM_ACTION_DECAY = 0.9999
    MIN_RANDOM_ACTION_PROB = 0.0
    FREEZE_POLICY = False
    SAMPLES_PER_EPISODES = 5

    random_action_prob = 0.8
   

    def __init__(self, env, meta_policy=None, alpha=0.001):
        self.reset_variables(env=env, alpha=alpha, meta_policy=None)
      

    def perform_step(self, update_meta):

        if self.action == None:
            self.state = self.env.reset()
            self.action, self.explore = self.select_action(self.state)
        obs, reward, done = self.env.step(self.action)
        state_next = obs

        action_next, explore_next = self.select_action(state_next)
        self.update_random_action_prob()

        # if self.FREEZE_POLICY == False:
        #     self.learning_algorithm.update(self.state, self.action, reward, state_next, action_next, etraces=False)
        
        if update_meta is True:
            self.meta_policy.sarsa_update(self.state, self.action, reward, state_next, action_next)


        update_info = {"state" : self.state.copy(), "action" : (self.action, self.explore), "reward" : reward,
                        "state_next" : state_next.copy(), "action_next" : (action_next, explore_next)} 
        
        self.state = state_next
        self.action = action_next
        self.explore = explore_next
        
        return reward, done, update_info
    

    def train(self, num_episodes=500, max_steps=1000, verbose=False, update_meta=False, render=False):

        total_steps = 0
        step_counts = []

        all_rewards = []

        for episode in range(num_episodes):
            self.state = self.env.reset()

            steps = 0
            episode_rewards = []
            self.action, self.explore = self.select_action(self.state)
            self.update_random_action_prob()
                
            sampled_trajectories = []
            for _ in range(self.SAMPLES_PER_EPISODES):
                trajectory = []        
                for step in range(max_steps):
                    # stdout.write("\rStep: %d - Episode: %d " %(step, episode))
                    # stdout.flush()
                    if render == True:
                        self.env.render()

                    reward, done, update_info = self.perform_step(update_meta)
                    
                    steps += 1
                    total_steps += 1
                    episode_rewards.append(reward)
                    trajectory.append( update_info )
                    if done:
                        break 

                sampled_trajectories.append( trajectory )
            
            self.learning_algorithm.update( sampled_trajectories )
            step_counts.append( steps ) 
            all_rewards.append( sum(episode_rewards) / self.SAMPLES_PER_EPISODES )
            mean_rewards = np.mean(all_rewards[-100:])
            if verbose and episode % 10 == 0:
                print("Training episode = {}, Total steps = {}, Last-100 mean reward = {}, Basis = {}, Alpha = {}"
                                                         .format(episode, total_steps, mean_rewards, self.env.order, self.learning_algorithm.alpha))


        return all_rewards

    
    def select_action(self, state):
        explore = False
        if random.random() < self.random_action_prob:
            if self.meta_policy is None:
                action = self.env.random_action()
            else:
                explore = True
                action = self.meta_policy.predict(state)
        else:
            action = self.learning_algorithm.select_action(state)
            

        return action, explore

    def update_random_action_prob(self):
        self.random_action_prob *= self.RANDOM_ACTION_DECAY
        if self.random_action_prob < self.MIN_RANDOM_ACTION_PROB:
            self.random_action_prob = self.MIN_RANDOM_ACTION_PROB
        

    def reset_variables(self, env, alpha, meta_policy=None):
        self.env = env
        self.env.reset()
        self.random_action_prob = 0.0
        self.meta_policy = meta_policy
        self.action = None

        # self.learning_algorithm = REINFORCE(alpha=0.001, beta=0.001, gamma=1.0, 
        #                             num_actions=self.env.num_actions, num_features=self.env.num_features)

        self.learning_algorithm = Sarsa(self.env.num_actions, self.env.num_features, alpha=alpha, gamma=0.9)
    

    def play(self):
        state = self.env.reset()
        done = False
        steps = 0
        while not done and steps < 200:
            self.env.render()
            action = self.learning_algorithm.select_action(self.state)
            state, _, done = self.env.step(action)

            steps += 1
        return steps