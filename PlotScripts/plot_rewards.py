import matplotlib.pyplot as plt
import numpy as np 
import sys 
import pickle 

rewards = pickle.load(open(sys.argv[1],"rb"))
directory = sys.argv[2]


for i, domain_rewards in enumerate(rewards):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_ylim([0, 100])
    mean_rewards = np.mean(domain_rewards, axis=0)
    ax.plot(range(len(mean_rewards)), mean_rewards, color="blue")
    ax.fill_between(range(len(mean_rewards)), mean_rewards+np.std(domain_rewards, axis=0),
                        mean_rewards-np.std(domain_rewards, axis=0), facecolor="blue", edgecolor="blue", alpha=0.4, interpolate=True)

    fig.savefig(directory+"/domain_"+str(i)+".png")
    plt.close('all')
