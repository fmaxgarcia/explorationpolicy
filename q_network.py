import gym
import random

import numpy as np
import tensorflow as tf
from gym import wrappers
import itertools


def modified_sigmoid(x):
      #return x normalized 0-1
      #Find values to normalize with sigmoid in range
      xout = x.copy()
      xout[0] = 1 / (1 + np.exp(-(0.5*x[0])))
      xout[1] = 1 / (1 + np.exp(-(0.002*x[1])))
      xout[2] = 1 / (1 + np.exp(-(5.0*x[2])))
      xout[3] = 1 / (1 + np.exp(-(0.002*x[3])))

      return xout

def normalize_features(x):
   #Limits taken from cartpole domain in gym environmnet
   cartpole_max = np.array([2.4*2, 1000., 12 * 2 * math.pi / 360, 1000.])
   cartpole_min = -cartpole_max.copy()
   cartpole_range = cartpole_max - cartpole_min
   return (x - cartpole_min) / cartpole_range



def fourier_basis(obs, order=2):
      #X is assumed to be normalized
      iter = itertools.product(range(order+1), repeat=obs.shape[0])
      multipliers = np.array([list(map(int,x)) for x in iter])

      basisFeatures = np.array([obs[i] for i in range(obs.shape[0])])
      basis = np.cos(np.pi * np.dot(multipliers, basisFeatures))
      return basis


class DQN:
   REPLAY_MEMORY_SIZE = 10000
   RANDOM_ACTION_DECAY = 0.99
   MIN_RANDOM_ACTION_PROB = 0.1
   HIDDEN1_SIZE = 20
   HIDDEN2_SIZE = 20
   NUM_EPISODES = 2000
   MAX_STEPS = 1000
   LEARNING_RATE = 0.01
   MINIBATCH_SIZE = 30
   DISCOUNT_FACTOR = 0.9
   TARGET_UPDATE_FREQ = 300
   REG_FACTOR = 0.001
   LOG_DIR = './tmp/'
   BASIS_ORDER = 1

   random_action_prob = 0.8
   replay_memory = []

   def __init__(self, env, session=None, meta_network=None):
      self.session = tf.Session() if session is None else session
      self.env = env
      self.meta_network = meta_network
      assert len(self.env.observation_space.shape) == 1
      self.env.reset()
      (obs, reward, done, info) = self.env.step(self.env.action_space.sample())
      obs = modified_sigmoid(obs)
      phi = fourier_basis(obs, order=self.BASIS_ORDER)

      self.input_size = phi.shape[0] #self.env.observation_space.shape[0]
      self.output_size = self.env.action_space.n
      self.init_network()
      self.session.run(tf.initialize_all_variables())
      
   def init_network(self):
      # Inference
      self.x = tf.placeholder(tf.float32, [None, self.input_size])
      with tf.name_scope('hidden1'):
         self.W1 = tf.Variable(
                         tf.truncated_normal([self.input_size, self.HIDDEN1_SIZE], 
                         stddev=0.01), name='W1')
         self.b1 = tf.Variable(tf.zeros(self.HIDDEN1_SIZE), name='b1')
         self.h1 = tf.nn.tanh(tf.matmul(self.x, self.W1) + self.b1)
      with tf.name_scope('hidden2'):
         self.W2 = tf.Variable(
                         tf.truncated_normal([self.HIDDEN1_SIZE, self.HIDDEN2_SIZE], 
                         stddev=0.01), name='W2')
         self.b2 = tf.Variable(tf.zeros(self.HIDDEN2_SIZE), name='b2')
         self.h2 = tf.nn.tanh(tf.matmul(self.h1, self.W2) + self.b2)
      with tf.name_scope('output'):
         self.W3 = tf.Variable(
                         tf.truncated_normal([self.HIDDEN2_SIZE, self.output_size], 
                         stddev=0.01), name='W3')
         self.b3 = tf.Variable(tf.zeros(self.output_size), name='b3')
         self.Q = tf.matmul(self.h2, self.W3) + self.b3
      self.weights = [self.W1, self.b1, self.W2, self.b2, self.W3, self.b3]

      # Loss
      self.targetQ = tf.placeholder(tf.float32, [None])
      self.targetActionMask = tf.placeholder(tf.float32, [None, self.output_size])
      # TODO: Optimize this
      q_values = tf.reduce_sum(tf.multiply(self.Q, self.targetActionMask), 
                           reduction_indices=[1])
      self.loss = tf.reduce_mean(tf.square(tf.subtract(q_values, self.targetQ)))

      # Reguralization
      for w in [self.W1, self.W2, self.W3]:
         self.loss += self.REG_FACTOR * tf.reduce_sum(tf.square(w))

      # Training
      optimizer = tf.train.AdamOptimizer(self.LEARNING_RATE)
      global_step = tf.Variable(0, name='global_step', trainable=False)
      self.train_op = optimizer.minimize(self.loss, global_step=global_step)

   def train(self, log_dir, num_episodes=NUM_EPISODES, verbose=False):

      # Summary for TensorBoard
      tf.summary.scalar('loss', self.loss)
      self.summary = tf.summary.merge_all()
      self.summary_writer = tf.summary.FileWriter(log_dir, self.session.graph)

      total_steps = 0
      step_counts = []

      target_weights = self.session.run(self.weights)
      all_rewards = []
      for episode in range(num_episodes):
         state = self.env.reset()
         state = modified_sigmoid(state)
         state = fourier_basis(state, order=self.BASIS_ORDER)

         steps = 0

         episode_rewards = []
         for step in range(self.MAX_STEPS):
            # Pick the next action and execute it
            action = None
            if random.random() < self.random_action_prob:
               if self.meta_network is None:
                  action = self.env.action_space.sample()
               else:
                  rand = random.random()
                  actions = self.meta_network.predict(state)
                  p_sum = 0.0
                  for idx, a in enumerate(actions):
                     p_sum += a 
                     if rand <= p_sum:
                        action = idx
                        break
            else:
               q_values = self.session.run(self.Q, feed_dict={self.x: [state]})
               action = q_values.argmax()
            self.update_random_action_prob()
            obs, reward, done, _ = self.env.step(action)
            obs = modified_sigmoid(obs)
            obs = fourier_basis(obs, order=self.BASIS_ORDER)

            episode_rewards.append( self.DISCOUNT_FACTOR**step * reward )

            # Update replay memory
            if done:
               reward = -100
            self.replay_memory.append((state, action, reward, obs, done))
            if len(self.replay_memory) > self.REPLAY_MEMORY_SIZE:
               self.replay_memory.pop(0)
            state = obs

            # Sample a random minibatch and fetch max Q at s'
            if len(self.replay_memory) >= self.MINIBATCH_SIZE:
               minibatch = random.sample(self.replay_memory, self.MINIBATCH_SIZE)
               next_states = [m[3] for m in minibatch]
               # TODO: Optimize to skip terminal states
               feed_dict = {self.x: next_states}
               feed_dict.update(zip(self.weights, target_weights))
               q_values = self.session.run(self.Q, feed_dict=feed_dict)
               max_q_values = q_values.max(axis=1)

               # Compute target Q values
               target_q = np.zeros(self.MINIBATCH_SIZE)
               target_action_mask = np.zeros((self.MINIBATCH_SIZE, self.output_size), dtype=int)
               for i in range(self.MINIBATCH_SIZE):
                  _, action, reward, _, terminal = minibatch[i]
                  target_q[i] = reward
                  if not terminal:
                     target_q[i] += self.DISCOUNT_FACTOR * max_q_values[i]
                  target_action_mask[i][action] = 1

               # Gradient descent
               states = [m[0] for m in minibatch]
               feed_dict = {
                  self.x: states, 
                  self.targetQ: target_q,
                  self.targetActionMask: target_action_mask,
               }
               _, summary = self.session.run([self.train_op, self.summary], 
                                                            feed_dict=feed_dict)

               # Write summary for TensorBoard
               if total_steps % 100 == 0:
                  self.summary_writer.add_summary(summary, total_steps)

            total_steps += 1
            steps += 1
            if done:
               break

         step_counts.append( steps ) 
         all_rewards.append( sum(episode_rewards) )
         mean_steps = np.mean(step_counts[-100:])
         if verbose and episode % 100 == 0:
            print("Training episode = {}, Total steps = {}, Last-100 mean steps = {}"
                                                         .format(episode, total_steps, mean_steps))

         # Update target network
         if episode % self.TARGET_UPDATE_FREQ == 0:
            target_weights = self.session.run(self.weights)

      return all_rewards

   def update_random_action_prob(self):
      self.random_action_prob *= self.RANDOM_ACTION_DECAY
      if self.random_action_prob < self.MIN_RANDOM_ACTION_PROB:
         self.random_action_prob = self.MIN_RANDOM_ACTION_PROB

   def play(self):
      state = self.env.reset()
      state = modified_sigmoid(state)
      state = fourier_basis(state, order=self.BASIS_ORDER)
      done = False
      steps = 0
      while not done and steps < 200:
         self.env.render()
         q_values = self.session.run(self.Q, feed_dict={self.x: [state]})
         action = q_values.argmax()
         state, _, done, _ = self.env.step(action)
         state = modified_sigmoid(state)
         state = fourier_basis(state, order=self.BASIS_ORDER)

         steps += 1
      return steps

   def reset_variables(self, env):
      self.env = env
      self.replay_memory = []
      self.random_action_prob = 0.8
      self.session.run(tf.assign(self.W1, np.random.normal(loc=0.0, scale=0.01, size=(self.input_size, self.HIDDEN1_SIZE)))) 
      self.session.run(tf.assign(self.b1, np.zeros((self.HIDDEN1_SIZE,))))

      self.session.run(tf.assign(self.W2, np.random.normal(loc=0.0, scale=0.01, size=(self.HIDDEN1_SIZE, self.HIDDEN2_SIZE)))) 
      self.session.run(tf.assign(self.b2, np.zeros((self.HIDDEN2_SIZE,))))

      self.session.run(tf.assign(self.W3, np.random.normal(loc=0.0, scale=0.01, size=(self.HIDDEN2_SIZE, self.output_size)))) 
      self.session.run(tf.assign(self.b3, np.zeros((self.output_size,))))



