import gym
import numpy as np
import matplotlib.pyplot as plt
import math
import itertools
import random
from threading import Thread
from linear_agent import LinearAgent
import os
import pickle
from LinearMeta import MetaPolicy
from Features import fourier_basis
from Environments import EnvWrapper
import sys
import time

lander_setup = [{"main_engine_power" : 13.0, "side_engine_power" : 0.6},            
                    {"main_engine_power" : 8.0, "side_engine_power" : 0.9},
                    {"main_engine_power" : 15.0, "side_engine_power" : 1.5},
                    {"main_engine_power" : 12.0, "side_engine_power" : 2.5}
                ]

alpha = 5e-5
basis_order = 2

UPDATE_ELIGIBILITY = 0
UPDATE_PARALLEL_SARSA = 1
UPDATE_NONE = 2

def thread_train(meta, alpha, idx, sample_rewards, sample_updates, agents, flags):
    agent = agents[idx]
    done = flags[idx]
    
    reward = 0.0
    update_info = None
    if done is False:
        reward, done, update_info = agent.perform_step(update_meta=False)
        flags[idx] = done
    
    
    sample_updates[idx] = update_info
    sample_rewards[idx] = reward

def _run_episode(agents, meta, num_samples, update=UPDATE_ELIGIBILITY, rand_prob=0.8, rand_decay=0.999, num_steps=1000):
    flags = []
    for a in range(len(agents)):
        flags.append(False)


    for a in agents:
        a.reset_variables(a.env, alpha=alpha, meta_policy=meta)
        a.random_action_prob = rand_prob
        a.RANDOM_ACTION_DECAY = rand_decay

    sample_updates = [ None ] * num_samples
    sample_rewards = [ None ] * num_samples
    all_rewards = []
    for steps in range(num_steps):

        threads = [ Thread(target=thread_train, args=(meta, alpha, idx, sample_rewards, sample_updates, agents, flags)) for idx in range(num_samples) ]
        for t in threads:
            t.start()

        for t in threads:
            t.join()

        all_rewards.append( list(sample_rewards) )
        


        if update == UPDATE_ELIGIBILITY:
            #Update based on sample averages with eligibility traces
            #######################################################
            for update_info in sample_updates:
                if update_info is None:
                    continue
                state = update_info["state"]
                action, explore = update_info["action"]
                reward = update_info["reward"]
                state_next = update_info["state_next"]
                action_next, explore_next = update_info["action_next"]

                if explore == True:
                    meta.sarsa_update_single(state, action, reward, state_next, action_next)
        elif update == UPDATE_PARALLEL_SARSA:
            ######### Update sarsa on all sample at once ################
            meta.sarsa_update( sample_updates )
    
    return all_rewards


def experiment_train_meta(save_directory, eligibility_traces=False):    

    env = gym.make('LunarLander-v2')
    env.reset()

    (obs, reward, done, info) = env.step(env.action_space.sample())
    obs = EnvWrapper.modified_sigmoid(obs)
    phi = fourier_basis(obs, order=basis_order)

    num_features = phi.shape[0]
    num_actions = env.action_space.n

    
    meta = MetaPolicy(num_features=num_features, num_actions=num_actions)

    if os.path.isdir(save_directory) == False:
        os.mkdir(save_directory)

    meta_train_episodes = 10
    num_episodes = 1000
    num_steps = 500
    for k in range(meta_train_episodes):
        environments, agents = [], []

        for i, setup in enumerate(lander_setup):
            gym_env = gym.make('LunarLander-v2')
            gym_env.env.MAIN_ENGINE_POWER = setup["main_engine_power"]
            gym_env.env.SIDE_ENGINE_POWER = setup["side_engine_power"]

            env = EnvWrapper(gym_env, setup, basis_order=basis_order)
            agent = LinearAgent(env, meta_policy=meta)

            environments.append( env )
            agents.append( agent )
            

        num_samples = len(environments)

        #Environments being created per thread. Delete previous environment creation.
        steps_per_episode = {}
        for ep in range(num_episodes):
            
            update_type = UPDATE_ELIGIBILITY if eligibility_traces==True else UPDATE_PARALLEL_SARSA
            
            start = time.time()
            all_rewards = _run_episode(agents=agents, meta=meta, 
                        num_samples=num_samples, update=update_type, num_steps=num_steps)
            stop = time.time()
            
            steps_per_episode[ep] = all_rewards


            domain_rewards = [list() for _ in range(num_samples)]  
            for d, ep_rewards in enumerate(steps_per_episode[ep]):
                for l in range(len(domain_rewards)):
                    domain_rewards[l].append(ep_rewards[l])


            print("================================")
            for d, rewards in enumerate(domain_rewards):
                print("Domain %d - Episode %d - Reward %d - Trial %d - Time: %f" %(d, ep, sum(rewards), k, round(stop-start,3)))
        
        print("Saving to: " + save_directory + "/meta_iter_"+str(k)+".pkl")
        pickle.dump({"meta":meta, "training_by_episode":steps_per_episode}, open(save_directory+"/meta_iter_"+str(k)+".pkl", "wb"))
            


def experiment_evaluate_fixed_meta(save_directory, meta):
    env = gym.make('LunarLander-v2')
    env.reset()

    (obs, reward, done, info) = env.step(env.action_space.sample())
    obs = EnvWrapper.modified_sigmoid(obs)
    phi = fourier_basis(obs, order=basis_order)

    num_features = phi.shape[0]
    num_actions = env.action_space.n


    if os.path.isdir(save_directory) == False:
        os.mkdir(save_directory)

    meta_train_episodes = 10
    num_episodes = 1000
    num_steps = 500
    for k in range(meta_train_episodes):
        environments, agents = [], []

        for i, setup in enumerate(lander_setup):
            gym_env = gym.make('LunarLander-v2')
            gym_env.env.MAIN_ENGINE_POWER = setup["main_engine_power"]
            gym_env.env.SIDE_ENGINE_POWER = setup["side_engine_power"]

            env = EnvWrapper(gym_env, setup, basis_order=basis_order)
            agent = LinearAgent(env, meta_policy=meta)

            environments.append( env )
            agents.append( agent )
            

        num_samples = len(environments)
        #Environments being created per thread. Delete previous environment creation.
        steps_per_episode = {}
        for ep in range(num_episodes):
            
            all_rewards = _run_episode(agents=agents, meta=meta, 
                    num_samples=num_samples, update=UPDATE_NONE, num_steps=num_steps)
        
            steps_per_episode[ep] = all_rewards

                
            domain_rewards = [list() for _ in range(len(environments))]  
            for d, ep_rewards in enumerate(steps_per_episode[ep]):
                for l in range(len(domain_rewards)):
                    domain_rewards[l].append(ep_rewards[l])


            print("================================")
            for d, rewards in enumerate(domain_rewards):
                print("Domain %d - Episode %d - Reward %d - Trial %d" %(d, ep, sum(rewards), k))
        
        pickle.dump({"meta":meta, "training_by_episode":steps_per_episode}, open(save_directory+"/meta_iter_"+str(k)+".pkl", "wb"))



def experiment_evaluate_exploit_learning(save_directory):
    env = gym.make('LunarLander-v2')
    env.reset()

    (obs, reward, done, info) = env.step(env.action_space.sample())
    obs = EnvWrapper.modified_sigmoid(obs)
    phi = fourier_basis(obs, order=basis_order)

    num_features = phi.shape[0]
    num_actions = env.action_space.n


    if os.path.isdir(save_directory) == False:
        os.mkdir(save_directory)


    meta_train_episodes = 100
    num_episodes = 200
    num_steps = 500
    
    meta = MetaPolicy(num_features=num_features, num_actions=num_actions)
    steps_per_meta_episode = {}
    for k in range(meta_train_episodes):
        environments, agents = [], []

        for i, setup in enumerate(lander_setup):
            gym_env = gym.make('LunarLander-v2')
            gym_env.env.MAIN_ENGINE_POWER = setup["main_engine_power"]
            gym_env.env.SIDE_ENGINE_POWER = setup["side_engine_power"]

            env = EnvWrapper(gym_env, setup, basis_order=basis_order)
            agent = LinearAgent(env, meta_policy=meta)

            environments.append( env )
            agents.append( agent )
            

        num_samples = len(environments)

        #Environments being created per thread. Delete previous environment creation.
        for ep in range(num_episodes):
            all_rewards = _run_episode(agents=agents, meta=meta, 
                num_samples=num_samples,update=UPDATE_ELIGIBILITY, num_steps=num_steps)
    


            domain_rewards = [list() for _ in range(len(environments))]  
            for d, ep_rewards in enumerate(all_rewards):
                for l in range(len(domain_rewards)):
                    domain_rewards[l].append(ep_rewards[l])

            print("================================")
            for d, rewards in enumerate(domain_rewards):
                print("Domain %d - Episode %d - Reward %d - Trial %d" %(d, ep, sum(rewards), k))
        


        all_rewards = _run_episode(agents=agents, meta=meta, num_samples=num_samples, update=UPDATE_NONE, rand_prob=0.0)

        steps_per_meta_episode[k] = list(all_rewards)
        
    pickle.dump({"meta":meta, "training_by_episode":steps_per_meta_episode}, open(save_directory+"/exploitation_only_results.pkl", "wb"))



def experiment_evaluate_exploration_learning(save_directory):
    env = gym.make('LunarLander-v2')
    env.reset()

    (obs, reward, done, info) = env.step(env.action_space.sample())
    obs = EnvWrapper.modified_sigmoid(obs)
    phi = fourier_basis(obs, order=basis_order)

    num_features = phi.shape[0]
    num_actions = env.action_space.n


    if os.path.isdir(save_directory) == False:
        os.mkdir(save_directory)


    meta_train_episodes = 100
    num_episodes = 200
    num_steps = 500
    
    meta = MetaPolicy(num_features=num_features, num_actions=num_actions)
    steps_per_meta_episode = {}
    for k in range(meta_train_episodes):
        environments, agents = [], []
        
        for i, setup in enumerate(lander_setup):
            gym_env = gym.make('LunarLander-v2')
            gym_env.env.MAIN_ENGINE_POWER = setup["main_engine_power"]
            gym_env.env.SIDE_ENGINE_POWER = setup["side_engine_power"]

            env = EnvWrapper(gym_env, setup, basis_order=basis_order)
            agent = LinearAgent(env, meta_policy=meta)
            agent.FREEZE_POLICY = True

            environments.append( env )
            agents.append( agent )
            

        num_samples = len(environments)
        #Environments being created per thread. Delete previous environment creation.
        for ep in range(num_episodes):
            all_rewards = _run_episode(agents=agents, meta=meta, num_samples=num_samples, update=UPDATE_ELIGIBILITY, num_steps=num_steps)
    

            domain_rewards = [list() for _ in range(len(environments))]  
            for d, ep_rewards in enumerate(all_rewards):
                for l in range(len(domain_rewards)):
                    domain_rewards[l].append(ep_rewards[l])

            print("================================")
            for d, rewards in enumerate(domain_rewards):
                print("Domain %d - Episode %d - Reward %d - Trial %d" %(d, ep, sum(rewards), k))
        


        for a in agents:
            a.FREEZE_POLICY = False
        
        all_rewards = _run_episode(agents=agents, meta=meta, num_samples=num_samples, update=UPDATE_ELIGIBILITY, rand_prob=0.0)

        steps_per_meta_episode[k] = list(all_rewards)
        
    pickle.dump({"meta":meta, "training_by_episode":steps_per_meta_episode}, open(save_directory+"/exploration_only_results.pkl", "wb"))
