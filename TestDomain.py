import gym
import cma
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import math
import itertools
import random

def modified_sigmoid(x):
    #return x normalized 0-1
    #Find values to normalize with sigmoid in range
    xout = x.copy()
    xout[0,0] = 1 / (1 + np.exp(-(0.5*x[0,0])))
    xout[0,1] = 1 / (1 + np.exp(-(0.002*x[0,1])))
    xout[0,2] = 1 / (1 + np.exp(-(5.0*x[0,2])))
    xout[0,3] = 1 / (1 + np.exp(-(0.002*x[0,3])))

    return xout


def fourier_basis(obs, order=2):
    #X is assumed to be normalized
    iter = itertools.product(range(order+1), repeat=obs.shape[1])
    multipliers = np.array([list(map(int,x)) for x in iter])

    basisFeatures = np.array([obs[0,i] for i in range(obs.shape[1])])
    basis = np.cos(np.pi * np.dot(multipliers, basisFeatures))
    return basis.reshape((1, basis.shape[0]))
    


def normalize_features(x):
    #Limits taken from cartpole domain in gym environmnet
    cartpole_max = np.array([2.4*2, 1000., 12 * 2 * math.pi / 360, 1000.])
    cartpole_min = -cartpole_max.copy()
    cartpole_range = cartpole_max - cartpole_min
    return (x - cartpole_min) / cartpole_range


cartpole_setup = [{"force" : 10.0, "pole_length" : 0.5, "masscart" : 1.0, "masspole" : 0.1}
                  # {"force" : 5.0, "pole_length" : 0.25, "masscart" : 2.0, "masspole" : 0.2},
                  # {"force" : 20.0, "pole_length" : 1.2, "masscart" : 5.0, "masspole" : 0.1},
                  # {"force" : 5.0, "pole_length" : 1.0, "masscart" : 2.0, "masspole" : 0.5},
                  # {"force" : 10.0, "pole_length" : 0.5, "masscart" : 5.0, "masspole" : 1.0},
                  ]

env = gym.make('CartPole-v0')




env.reset()
(obs, reward, done, info) = env.step(env.action_space.sample())
phi = obs.reshape((1, obs.shape[0]))
# obs = modified_sigmoid(obs)
# phi = fourier_basis(obs, order=3)
num_features = phi.shape[1]
num_actions = env.action_space.n

num_hidden = num_features // 2
inputs = tf.placeholder(shape=[None,num_features], dtype=tf.float32)
W1 = tf.Variable(tf.random_normal([num_features, num_hidden], 0, 5.0))
b1 = tf.Variable(tf.random_normal([num_hidden], 0, 5.0))

h1 = tf.nn.softmax( tf.add(tf.matmul(inputs, W1), b1) )

W2 = tf.Variable(tf.random_normal([num_hidden, num_actions], 0, 5.0))
b2 = tf.Variable(tf.random_normal([num_actions], 0, 5.0))

Q = tf.add(tf.matmul(h1, W2), b2)

nextQ = tf.placeholder(shape=[None,num_actions], dtype=tf.float32)
loss = tf.reduce_sum(tf.square(nextQ - Q))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
updateModel = optimizer.minimize(loss)

episodes = 1000
session = tf.Session()
epsilon = 0.8
gamma = 0.99

memory = []
mem_index = 0

for setup in cartpole_setup:
    env = gym.make('CartPole-v0')

    sum_rewards = []
    for i in range(episodes):
        session.run(tf.global_variables_initializer())
        obs = env.reset()
        env.env.force_mag = setup["force"]
        env.env.length = setup["pole_length"]
        env.env.masscart = setup["masscart"]
        env.env.masspole = setup["masspole"]
        phi = obs.reshape((1, obs.shape[0]))

        # obs = modified_sigmoid(obs)
        # phi = fourier_basis(obs, order=3)
        done = False
        r = 0
        while done == False:

            Qs = session.run(Q, feed_dict={inputs:phi})
            if np.random.rand(1) < epsilon:
                a = env.action_space.sample()
            else:
                a = np.argmax(Qs)

            (newobs, reward, done, info) = env.step(a)
            phi_next = newobs.reshape((1, newobs.shape[0]))
            # newobs = modified_sigmoid(newobs)
            # phi_next = fourier_basis(newobs, order=3)

            Qsnext = session.run(Q, feed_dict={inputs:phi_next})
            maxq = np.max(Qsnext)

            target = Qs.copy()
            target[0, a] = reward + gamma * maxq
            r += reward

            ########### Use replay memory ################
            if len(memory) > 10000:
                memory[mem_index] = (phi, target)
                mem_index = (mem_index + 1) % 10000
            else:
                memory.append( (phi, target) )

            indices = range(len(memory))
            random.shuffle(indices)

            selected_indices = indices if len(indices) < 128 else indices[:128]

            P, T = None, None
            for k in selected_indices:
                if P is None:
                    P, T = memory[k]
                else:
                    p, t = memory[k]
                    P = np.vstack( (P, p) )
                    T = np.vstack( (T, t) )

            session.run([updateModel], feed_dict={inputs:P, nextQ:T})

            phi = phi_next
        print("Episode: " + str(i) + " - " + str(r))
        sum_rewards.append( r )
        epsilon *= 0.99

plt.plot(range(len(sum_rewards)), sum_rewards)
plt.show(block=True)
